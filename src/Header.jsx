import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Logo from './assets/cropped-Langvik_logo_transp_musta.png'
import CustomizedMenus from './Menu';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
    color: 'black'
  },
  title: {
    flexGrow: 1,
    color: 'black',
  },
  logo: {
    flexGrow: 1,
    maxWidth: 150,
  },
  customizeToolbar: {
    backgroundColor: "white"
  },
  login: {
    color: 'black',
  },
}));

export default function Header() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.customizeToolbar}>

          <Button color="inherit" href="http://www.langvik.fi/">
          <img src={Logo} alt="logo" className={classes.logo} />
          </Button>
          
          <Typography variant="h6" className={classes.title}></Typography>

          <CustomizedMenus />
          <Button color="inherit" className={classes.login}>VARAUKSET</Button>

        </Toolbar>
      </AppBar>
    </div>
  );
}